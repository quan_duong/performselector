//
//  DemoFunction.m
//  ObjCExamples
//
//  Created by Duong Tien Quan on 11/7/15.
//  Copyright © 2015 vtvcab. All rights reserved.
//

#import "DemoFunction.h"

@interface DemoFunction ()

@end

@implementation DemoFunction

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self writeln:@"Demo funciton"];
    [self writeln:@"Hello World"];
    [self sayFirstName:@"Steve" andLastName:@"Jobs"];
    float tempDegree = 30.1;
    NSString* result = [NSString stringWithFormat:@"%2.1f degree equal to %3.1f",tempDegree,[self celciusToFahrenheight:tempDegree]];
    [self writeln:result];
    [self performSelector:@selector(celciusToFahrenheight2:) withObject:@(30.1)];
}

-(void) sayFirstName:(NSString*) firstName andLastName:(NSString*) lastName{
    NSLog(@"%@ %@",firstName,lastName);
}

-(float) celciusToFahrenheight: (float)degree{
    return degree * 1.8+ 32.0;
}
-(void) celciusToFahrenheight{
    NSLog(@"Do nothing");
}

-(float) celciusToFahrenheight2{
    return 10.2;
}

-(void) celciusToFahrenheight2:(NSNumber*) degree{
    
    float result = [degree floatValue] * 1.8 + 32.0;
    [self writeln:[[NSNumber numberWithFloat:result] stringValue]];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    }


@end
